expires_at_dates = ["2024-10-15","2024-10-16","2024-10-17"]


puts "token type;project/group id;project/group path;token id;token name;scopes;last used"


expires_at_dates.each do |expires_at_date|
  PersonalAccessToken.project_access_token.where(expires_at: expires_at_date).find_each do |token|
    token.user.members.each do |member|
          type = member.is_a?(GroupMember) ? 'Group' : 'Project'
          path = ''
          if member.is_a?(GroupMember)
             group = Group.by_id(member.source_id)[0]
             path = group.full_path
          else member.is_a?(ProjectMember)
             project = Project.find_by(id: member.source_id)
             path = project.full_path
          end

      puts "#{type};#{member.source_id};#{path};#{token.id};#{token.name};#{token.scopes};#{token.last_used_at}"
    end
  end
end