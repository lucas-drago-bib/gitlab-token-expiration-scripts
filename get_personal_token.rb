expires_at_dates = ["2024-10-15", "2024-10-16", "2024-10-17"]

puts "Firstname lastname username;Token Name;Token id;Scopes;Last used"
expires_at_dates.each do |expires_at_date|
  PersonalAccessToken.owner_is_human.where(expires_at: expires_at_date, revoked: false).find_each do |token|
     if token.user.members[0]
       user = User.find(token.user.members[0].user_id)
       puts "#{user.first_name} #{user.last_name} (#{user.username});#{token.name};#{token.id};#{token.scopes};#{token.last_used_at}"
     end
  end
end
