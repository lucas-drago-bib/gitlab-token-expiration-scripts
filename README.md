# Introduction

This small documentation has been created to help you to identify token from a GitLab instance that will expire on a defined date.

## Identify personal tokens that will expire

Copy on your GitLab instance the file `git_personal_token.rb` and adjust the variable `expires_at_dates` to the dates you want to check. Then run the script with the following command:

```bash
  sudo gitlab-rails runner /path_of_script/get_personal_token.rb > /tmp/expired_personal_tokens.csv
```

In the file `/tmp/expired_personal_tokens.csv` you will find the list of personal tokens that will expire on the dates you have defined.

## Identify group/project tokens that will expire

Copy on your GitLab instance the file `get_project_group_token.rb` and adjust the variable `expires_at_dates` to the dates you want to check. Then run the script with the following command:

```bash
  sudo gitlab-rails runner /tmp/get_project_group_token.rb > /tmp/expired_project_group_tokens.csv
```

In the file `/tmp/expired_project_group_tokens.csv` you will find the list of group/project tokens that will expire on the dates you have defined.

